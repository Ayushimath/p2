const userData=require('./p2');

function fullName(eachUser){
   let first=eachUser.first_name;
   let last=eachUser.last_name;
   let fullname=first+" "+last;
   eachUser["full_name"]=fullname;

   return eachUser;
}

const modifiedUserData=userData.map(fullName);
console.log(modifiedUserData);