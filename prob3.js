const userData=require('./p2');

function ipTwoSum(ip_two_sum,eachUser){
    let ip_address=eachUser.ip_address;
    ip_address=ip_address.split(".");
    ip_address=ip_address.map((num)=>parseInt(num));

    ip_two_sum+=ip_address[1];
    return ip_two_sum;
}

const iptwosum=userData.reduce(ipTwoSum,0);
console.log(iptwosum);