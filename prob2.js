const userData=require('./p2');

function splitIp(eachUser){
    let ip_address=eachUser.ip_address;
    ip_address=ip_address.split(".");
    ip_address=ip_address.map((num)=>parseInt(num));

    eachUser["ip_address"]=ip_address;
    return eachUser;
}

const ipaddrsplit=userData.map(splitIp);
console.log(ipaddrsplit);